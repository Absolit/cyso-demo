export interface ContactStoreInterface {
    personResponse: {
        code: number
        data: ContactInterface[],
        status: string,
        total: number,
    } | null | Ref,
    contacts: ContactInterface[] | [],
    loading: boolean | Ref,
    search: string,
    sort: string,
    sortOptions: SortOptionInterface[],
    perPage: number,
    activePage: number,
}

export interface ContactInterface {
    id: number;
    address: {
        address: string;
        buildingNumber: string;
        city: string;
        country: string;
        county_code: string;
        id: number;
        latitude: number;
        longitude: number;
        street: string;
        streetName: string;
        zipcode: string;
    };
    birthday: string;
    email: string;
    firstname: string;
    lastname: string;
    gender: string;
    image: string;
    phone: string;
    website: string;
}

export interface ContactStatusInterface {
    label: string,
    value: string,
}

export interface SortOptionInterface {
    label: string,
    value: string,
}