import {resolve} from 'path'

export default defineNuxtConfig({
    devtools: {enabled: true},
    modules: [
        '@pinia/nuxt',
    ],
    ssr: true,
    app: {
        head: {
            link: [
                {
                    rel: "stylesheet",
                    href: "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap",
                },
            ],
        },
    },
    vite: {
        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: '@import "@/assets/scss/config/variables.scss";',
                },
            },
        },
    },
    css: [
        '@fortawesome/fontawesome-svg-core/styles.css',
        "~/assets/scss/app.scss",
    ],
    plugins: [
        '~/plugins/font-awesome.js',
    ],
    build: {
        transpile: ['@fortawesome/vue-fontawesome']
    },
    alias: {
        '@img': resolve(__dirname, 'assets/images')
    },
})
