import {defineStore} from "pinia";
import {ContactInterface, ContactStoreInterface} from "~/types/contact";


export const useContactStore = defineStore('contacts', {
    state: (): ContactStoreInterface => ({
        personResponse: null,
        contacts: [],
        loading: false,
        search: '',
        sort: 'id:asc',
        sortOptions: [
            {value: 'id:asc', label: 'ID Oplopend'},
            {value: 'id:desc', label: 'ID Aflopend'},
            {value: 'firstname:asc', label: 'Voornaam Oplopend'},
            {value: 'firstname:desc', label: 'Voornaam Aflopend'},
            {value: 'lastname:asc', label: 'Achternaam Oplopend'},
            {value: 'lastname:desc', label: 'Achternaam Aflopend'},
        ],
        perPage: 12,
        activePage: 1,
    }),
    actions: {
        async fetchContacts() {
            const url = 'https://fakerapi.it/api/v1/persons';
            const query = {
                _locale: 'nl-NL',
                _seed: 'cyso',
                _quantity: 500,
                _birthday_start: '1960-01-01',
                _birthday_end: '2003-01-01',
            };

            try {
                console.log("[Cyso]", "Retrieving contacts through API...");
                const { data, pending } = await useFetch(url, { query });
                this.personResponse = data;
                this.loading = pending;
                this.contacts = this?.personResponse?.data || [];

                console.log('[Cyso]', `Got ${this.personResponse?.total || 0} contacts`);
            } catch (error) {
                console.error('[Cyso]', error);
            }
        },
        async deleteContactById(contactId: number) {
            try {
                const contactIndex = this.contacts.findIndex((c) => c.id === contactId);

                if (contactIndex !== -1) {
                    this.contacts.splice(contactIndex, 1);
                }

                console.log('[Cyso]', `Contact with ID ${contactId} has been deleted`);
            } catch (error) {
                console.error('[Cyso]', `Error deleting contact with ID ${contactId}:`, error);
            }
        },
    },
    getters: {
        count(state): number {
            const computedContacts = this.computedContacts;
            return computedContacts?.length || 0;
        },
        computedContacts(state): ContactInterface[] {
            const { search, contacts } = state;

            return contacts.filter((contact: ContactInterface) => {
                const keysToSearch: (keyof ContactInterface)[] = ['firstname', 'lastname', 'email', 'phone'];

                return keysToSearch.some((key) => {
                    const value = contact[key]?.toString().toLowerCase();
                    return value.includes(search.toLowerCase());
                });
            });
        },
        paginatedContacts(state): ContactInterface[] {
            const {activePage, perPage, sort} = state;
            const computedContacts = this.computedContacts;
            const [field, order] = sort.split(':');

            return computedContacts
                .sort((a: ContactInterface, b: ContactInterface) => {
                    const fieldValueA = a[field as keyof ContactInterface];
                    const fieldValueB = b[field as keyof ContactInterface];

                    if (typeof fieldValueA === 'string' && typeof fieldValueB === 'string') {
                        return order === 'asc' ? fieldValueA.localeCompare(fieldValueB) : fieldValueB.localeCompare(fieldValueA);
                    }

                    const numericA = Number(fieldValueA);
                    const numericB = Number(fieldValueB);

                    return order === 'asc' ? numericA - numericB : numericB - numericA;
                })
                .slice((activePage - 1) * perPage, activePage * perPage);
        },
        getContactById: (state) => {
            return (cID: number) => {
                const { contacts } = state;
                return contacts.find(c => c.id === cID)
            };
        },
    }
})